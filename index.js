(function () {
    "use strict";
    /*
     * index.js
     * @Author: Brenton O'Callaghan
     * @Date: 19th February 2018
     * @Description: Simple test webserver program \
     */

    /*global console, require*/

    const express = require('express');
    const app = express();

    // Handles a basic web request for a quote.
    var responseHandler = function(req, res){
        console.log("GET request received");
        res.send('A bird in the hand is worth two in the bush!')
    };

    // Handles the request for the app to quit
    // Really un-necessary but in place to allow for testing of docker restarts
    var quitHandler = function(req, res){
        console.log("GET /quit request received");
        res.send('Quitting')
        process.exit(0);
    };

    // Attach the handlers to the right GET requests
    app.get('/', responseHandler);
    app.get('/quit', quitHandler);

    // Start the server listening on port 3000.
    app.listen(3000, () => console.log('Inspirational quotes app listening on port 3000'));
}());
