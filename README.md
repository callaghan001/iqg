# README #

This repo contains source code that demonstrates a simple node.js program that has the ability to be contained within a docker image and deployed in the same way. 

### The node.js program ###
The node portion is a very simple Node.js program which runs a webserver on port 300 and responds to GET requests at:
* / - returns a quotation in a simple text form
* /quit - triggers the underlying webserver running in node to quit

You can run the program as follows (after cloning the repo):
> npm install
> node index.js

### The Dockerfile ###

The Dockerfile file contains the build settings to be used during the docker image build. For more info on how these work please see docker.com